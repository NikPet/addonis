package addonisproject.addonis.controllers;

import addonisproject.addonis.addonsDataBase.models.Mapper;
import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonCardDTO;
import addonisproject.addonis.addonsDataBase.services.contracts.AddonsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static addonisproject.addonis.utils.Constants.*;

/**
 * The search Controller is responsible for handling user search request.
 * It has single method, which receives query parameters and sends them to
 * the Service layer. It returns information about the addons matching the search criteria,
 * or returns error if no addon matches the search criteria.
 */
@Controller
@RequestMapping("/search")
public class SearchController {

    private AddonsService addonsService;
    private Logger logger;
    private FileHandler fh;
    private Mapper mapper;

    @Autowired
    public SearchController(AddonsService addonsService, Mapper mapper) throws IOException {
        this.addonsService = addonsService;
        this.mapper = mapper;
        logger = Logger.getLogger(SearchController.class.getSimpleName());
        fh = new FileHandler(LOG_FOLDER + "Search.log");
        logger.addHandler(fh);
    }

    @GetMapping
    public String searchAddons(@RequestParam(name = "ide", required = false) String ide,
                               @RequestParam(name = "name", required = false) String name,
                               @RequestParam(name = "sort", required = false) String sortCategory,
                               @RequestParam(name = "page", defaultValue = "1") Integer page,
                               Model model) {
        List<Addon> result = addonsService.searchAddons(ide, name, sortCategory);
        List<AddonCardDTO>resultCards= mapper.addonListToAddonCardList(result);
        try {
            if (addonsService.paginate(resultCards,page).size() == 0) {
                throw new EntityNotFoundException(
                        String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE,
                                NO_ADDONS_MATCH_CRITERIA_ERROR_MESSAGE));
            }
            model.addAttribute("addons", addonsService.paginate(resultCards,page));
            model.addAttribute("pages", page);
            model.addAttribute("shouldShowNext",result.size()>page*PER_PAGE_SEARCH);
            model.addAttribute("shouldShowPrevious",page>1);
            return "search-page";
        } catch (EntityNotFoundException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getStackTrace());
            model.addAttribute("errors", e.getMessage());
            return "error";
        }
    }
}
