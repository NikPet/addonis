package addonisproject.addonis.controllers.rest;

import addonisproject.addonis.addonsDataBase.services.contracts.AddonsService;
import addonisproject.addonis.addonsDataBase.services.contracts.RatingService;
import addonisproject.addonis.addonsDataBase.services.contracts.TagsService;
import addonisproject.addonis.addonsDataBase.services.contracts.UsersService;
import addonisproject.addonis.exceptions.DuplicateEntityException;
import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.Mapper;
import addonisproject.addonis.addonsDataBase.models.addons.Rating;
import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonDTO;
import addonisproject.addonis.addonsDataBase.models.dtos.RatingDTO;
import addonisproject.addonis.addonsDataBase.models.dtos.TagDTO;
import addonisproject.addonis.exceptions.FileProblemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;

import static addonisproject.addonis.utils.ValidatorHelper.isAdmin;
import static addonisproject.addonis.utils.ValidatorHelper.isCreator;

/**
 * The class delivers the Api functionality related to addons - including all CRUD operations
 */
@RestController
@RequestMapping("/api/addons")
public class AddonsRestController {

    private AddonsService addonsService;
    private UsersService usersService;
    private TagsService tagsService;
    private Mapper mapper;
    private RatingService ratingService;

    @Autowired
    public AddonsRestController(AddonsService addonsService,
                                UsersService usersService,
                                TagsService tagsService,
                                Mapper mapper,
                                RatingService ratingService) {
        this.addonsService = addonsService;
        this.usersService = usersService;
        this.tagsService = tagsService;
        this.mapper = mapper;
        this.ratingService = ratingService;
    }

    @GetMapping
    public List<Addon> getAll() {
        return addonsService.getAll();
    }

    @GetMapping("/{id}")
    public Addon getById(@PathVariable int id) {
        try {
            return addonsService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/name")
    public Addon getByName(@RequestParam("name") String name) {
        try {
            return addonsService.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<Addon> searchByName(@RequestParam("name") String name) {
        return addonsService.searchByName(name);
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/new")
    public Addon createAddon(@RequestBody AddonDTO addonDTO, Principal principal) {
        try {
            Addon addonToCreate = new Addon();
            addonToCreate = mapper.dtoToAddon(addonDTO, addonToCreate);
            UserDetails creator = usersService.getUserByName(principal.getName());
            addonToCreate.setCreator(creator);
            addonToCreate.setUploadDate(new Date());
            addonsService.createAddon(addonToCreate);
            return addonToCreate;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (FileProblemException e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/{id}/rate")
    public Rating rateAddon(@PathVariable int id, @Valid @RequestBody RatingDTO ratingDTO, Principal principal) {
        try {
            Addon addon = addonsService.getById(id);
            UserDetails user = usersService.getUserByName(principal.getName());
            Rating rating = new Rating();
            rating.setAddon(addon);
            rating.setUserDetails(user);
            rating.setRating(ratingDTO.getRating());
            ratingService.rateAddon(rating);
            return rating;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/average")
    public double getAverage(@PathVariable int id) {
        try {
            Addon addon = addonsService.getById(id);
            return ratingService.getAvgRating(addon);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PutMapping("/{id}")
    public Addon updateAddon(@PathVariable int id,
                             @RequestBody @Valid AddonDTO addonDTO, Principal principal) {
        try {
            Addon addon = addonsService.getById(id);
            addon = mapper.dtoToAddon(addonDTO, addon);
            UserDetails user = usersService.getUserByName(principal.getName());
            if (!isCreator(addon, user) && !isAdmin()) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }
            addonsService.updateAddon(addon);
            return addon;
        } catch (FileProblemException e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @DeleteMapping("/{id}")
    public void deleteAddon(@PathVariable int id, Principal principal) {
        try {
            Addon addon = addonsService.getById(id);
            UserDetails user = usersService.getUserByName(principal.getName());
            if (!isCreator(addon, user) && !isAdmin()) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }
            addonsService.deleteAddon(addon);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/{id}/tag")
    public Addon addTag(@PathVariable int id, @RequestBody @Valid TagDTO tagDTO, Principal principal) {
        try {
            UserDetails creator = usersService.getUserByName(principal.getName());
            Addon addon = addonsService.getById(id);
            if (!isCreator(addon, creator) && !isAdmin()) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }
            tagsService.addTagToAddon(addon, tagDTO);
            return addon;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @DeleteMapping("/{id}/tag")
    public Addon removeTag(@PathVariable int id, @RequestBody @Valid TagDTO tagDTO, Principal principal) {
        try {
            UserDetails creator = usersService.getUserByName(principal.getName());
            Addon addon = addonsService.getById(id);
            if (!isCreator(addon, creator) && !isAdmin()) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }
            tagsService.removeTagFromAddon(addon, tagDTO);
            return addon;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
