package addonisproject.addonis.addonsDataBase.services.contracts;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonCardDTO;

import java.util.List;

public interface AddonsService {

    List<Addon> getAll();

    List<Addon> getAllStatusApproved();

    List<Addon> getAllStatusPending();

    Addon getById(int id);

    Addon getByName(String name);

    List<Addon> searchByName(String name);

    void createAddon(Addon addon);

    void updateNumberDownloadsOnClick(int id);

    void updateAddon(Addon addon);

    void deleteAddon(Addon addon);

    void approveAddon(Addon addon);

    List<Addon> getFeaturedAddons();

    void addToFeatured(Addon addon);

    void removeFromFeatured(Addon addon);

    List<Addon> getPopularAddons();

    List<Addon> getNewestAddons();

    List<Addon> getUserApprovedAddons(int id);

    List<Addon> getUserPendingAddons(int id);

    List<Addon> searchAddons(String ide, String name, String sortCategory);

    List<AddonCardDTO> paginate(List<AddonCardDTO> addonList, int page);
}
