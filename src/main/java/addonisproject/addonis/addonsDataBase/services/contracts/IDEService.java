package addonisproject.addonis.addonsDataBase.services.contracts;

import addonisproject.addonis.addonsDataBase.models.addons.IDE;

import java.util.List;

public interface IDEService {

    List<IDE> getAll();

    IDE getIdeByName(String name);
}
