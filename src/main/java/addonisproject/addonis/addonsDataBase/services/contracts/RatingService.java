package addonisproject.addonis.addonsDataBase.services.contracts;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.Rating;

public interface RatingService {

    void rateAddon(Rating rating);

    double getAvgRating(Addon addon);
}
