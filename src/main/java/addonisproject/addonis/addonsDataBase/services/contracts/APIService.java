package addonisproject.addonis.addonsDataBase.services.contracts;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.Commit;

public interface APIService {

     Commit getLastCommit(Addon addon);

     int getCountPulls(Addon addon);

     int getCountIssues(Addon addon);

     String getReadMe(Addon addon);
}
