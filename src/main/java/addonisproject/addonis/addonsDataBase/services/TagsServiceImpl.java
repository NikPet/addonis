package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.models.addons.Tag;
import addonisproject.addonis.addonsDataBase.models.dtos.TagDTO;
import addonisproject.addonis.addonsDataBase.repos.AddonsRepository;
import addonisproject.addonis.addonsDataBase.repos.TagsRepository;
import addonisproject.addonis.addonsDataBase.services.contracts.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import static addonisproject.addonis.utils.Constants.ENTITY_NOT_FOUND_ERROR_MESSAGE;
import static addonisproject.addonis.utils.Constants.TAG_TYPE;

/**
 * Implementation of TagService interface, used for CRUD operations, adding and removing Tag form Addon
 */

@Service
public class TagsServiceImpl implements TagsService {

    private TagsRepository tagsRepository;
    private AddonsRepository addonsRepository;

    @Autowired
    public TagsServiceImpl(TagsRepository tagsRepository, AddonsRepository addonsRepository) {
        this.tagsRepository = tagsRepository;
        this.addonsRepository = addonsRepository;
    }

    @Override
    public Tag getTagByName(String name) {
        return tagsRepository.getByName(name)
                .orElseThrow(() -> new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE, TAG_TYPE)));
    }

    @Override
    public void addTagToAddon(Addon addon, TagDTO tagDTO) {
        Tag tagToAdd = new Tag();
        if (tagsRepository.checkTagNameExists(tagDTO.getName())) {
            tagToAdd = getTagByName(tagDTO.getName());
        } else {
            tagToAdd.setName(tagDTO.getName());
            tagsRepository.save(tagToAdd);
        }
        addon.getTags().add(tagToAdd);
        addonsRepository.save(addon);
    }

    @Override
    public void removeTagFromAddon(Addon addon, TagDTO tagDTO) {
        Tag tag;
        if (tagsRepository.checkTagNameExists(tagDTO.getName())) {
            tag = getTagByName(tagDTO.getName());
        } else {
            throw new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE, TAG_TYPE));
        }
        if (!addon.getTags().contains(tag)) {
            throw new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE, TAG_TYPE));
        }
        addon.getTags().remove(tag);
        addonsRepository.save(addon);
    }
}
