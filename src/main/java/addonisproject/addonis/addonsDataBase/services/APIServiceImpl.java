package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.Commit;
import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.services.contracts.APIService;
import addonisproject.addonis.addonsDataBase.services.serviceUtils.GitHubConnector;
import addonisproject.addonis.exceptions.ApiConnectionException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static addonisproject.addonis.addonsDataBase.services.serviceUtils.URLBuilder.createAPIUrl;
import static addonisproject.addonis.utils.Constants.*;

/**
 * APIServiceImpl is an implementation of the API service interface.
 * The class is used to retrieve only the necessary data from GitHub responses.
 * The class receives JSONArray from the JsonReader helper class and manipulates it
 * in order to get the requested by the controller data.
 */
@Service
public class APIServiceImpl implements APIService {

    public APIServiceImpl() {
    }

    @Override
    public Commit getLastCommit(Addon addon) {
        String originLink = addon.getOriginLink();
        String APIUrl = createAPIUrl(originLink, COMMITS);
        try {
            JSONArray JCommits = GitHubConnector.readJsonFromUrl(APIUrl);
            JSONObject JObject = JCommits.getJSONObject(0);
            String stringDate = JObject.getJSONObject("commit").getJSONObject("author").getString("date");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = dateFormat.parse(stringDate);
            String message = JObject.getJSONObject("commit").getString("message");
            return new Commit(date, message);
        } catch (JSONException | IOException | ParseException e) {
            throw new ApiConnectionException(CONNECTION_ERROR_MESSAGE);
        }
    }

    @Override
    public int getCountPulls(Addon addon) {
        String originLink = addon.getOriginLink();
        return getCount(originLink, PULLS);
    }

    @Override
    public int getCountIssues(Addon addon) {
        String originLink = addon.getOriginLink();
        return getCount(originLink, ISSUES);
    }

    @Override
    public String getReadMe(Addon addon) {
        String originLink = addon.getOriginLink();
        String APIUrl = createAPIUrl(originLink, README);
        try {
            String newURL = GitHubConnector.readJsonObjectFromUrl(APIUrl).getString("download_url");
            return GitHubConnector.getReadMeFromURL(newURL);
        } catch (JSONException | IOException e) {
            throw new ApiConnectionException(CONNECTION_ERROR_MESSAGE);
        }
    }

    private int getCount(String originLink, String type) {
        String APIUrl;
        int count = 0;
        try {
            JSONArray JPulls;
            int page = BEGINNING_PAGE;
            do {
                APIUrl = createAPIUrl(originLink, type, page, PER_PAGE);
                JPulls = GitHubConnector.readJsonFromUrl(APIUrl);
                count += JPulls.length();
                page++;
            } while (JPulls.length() != 0);
        } catch (IOException | JSONException e) {
            throw new ApiConnectionException(CONNECTION_ERROR_MESSAGE);
        }
        return count;
    }
}
