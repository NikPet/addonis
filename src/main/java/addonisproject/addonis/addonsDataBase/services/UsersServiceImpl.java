package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.exceptions.UserRegistrationException;
import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import addonisproject.addonis.addonsDataBase.repos.UserDetailsRepository;
import addonisproject.addonis.addonsDataBase.services.contracts.UsersService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static addonisproject.addonis.utils.Constants.*;

/**
 * Implementation of UserService interface, used for CRUD operations and retrieving information by
 * different criteria about users from the database
 */

@Service
public class UsersServiceImpl implements UsersService {

    private UserDetailsRepository userDetailsRepository;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;

    public UsersServiceImpl(UserDetailsRepository userDetailsRepository,
                            UserDetailsManager userDetailsManager,
                            PasswordEncoder passwordEncoder) {
        this.userDetailsRepository = userDetailsRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void createUser(UserDetails userDetails, String password, String repeatPassword) {
        if (isUsernameTaken(userDetails.getUsername())) {
            throw new UserRegistrationException(USERNAME_ALREADY_EXISTS_ERROR_MESSAGE);
        }
        if (isEmailTaken(userDetails.getEmail())) {
            throw new UserRegistrationException(THIS_EMAIL_ALREADY_REGISTERED_ERROR_MESSAGE);
        }
        if (!password.equals(repeatPassword)) {
            throw new UserRegistrationException(PASSWORDS_MISMATCH_ERROR_MESSAGE);
        }
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(userDetails.getUsername(),
                        passwordEncoder.encode(password), authorities);
        userDetailsManager.createUser(newUser);
        userDetailsRepository.save(userDetails);
    }

    @Override
    public UserDetails getUserByName(String name) {
        return userDetailsRepository.getByName(name)
                .orElseThrow(() -> new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE, USER_TYPE)));
    }

    @Override
    public UserDetails getUserById(int id) {
        return userDetailsRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_ERROR_MESSAGE, USER_ID)));
    }

    @Override
    public List<UserDetails> getAllUsers() {
        return userDetailsRepository.findAll();
    }

    @Override
    public void deleteUser(UserDetails userDetails) {
        userDetails.setEnabled(false);
        userDetailsRepository.save(userDetails);
    }

    @Override
    public List<UserDetails> getAllNonAdmins() {
        List<UserDetails> allUsers = getAllUsers();
        List<UserDetails> resultUser = new ArrayList<>();
        for (UserDetails user : allUsers) {
            org.springframework.security.core.userdetails.UserDetails userDetails;
            userDetails = userDetailsManager.loadUserByUsername(user.getUsername());
            if (userDetails
                    .getAuthorities()
                    .stream()
                    .noneMatch(r -> r.getAuthority().equals("ROLE_ADMIN"))) {
                resultUser.add(user);
            }
        }
        return resultUser;
    }

    private boolean isUsernameTaken(String newUserName) {
        List<String> userNames = getAllUsers().stream().map(UserDetails::getUsername).collect(Collectors.toList());
        return userNames.stream().anyMatch(names -> names.equals(newUserName));
    }

    private boolean isEmailTaken(String email) {
        List<String> userNames = getAllUsers().stream().map(UserDetails::getEmail).collect(Collectors.toList());
        return userNames.stream().anyMatch(names -> names.equals(email));
    }
}
