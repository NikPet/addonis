package addonisproject.addonis.addonsDataBase.repos;

import addonisproject.addonis.addonsDataBase.models.addons.IDE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The IDERepository is responsible for retrieving predefined collections of IDEs
 * from the ides table.
 *
 */
@Repository
public interface IDERepository extends JpaRepository<IDE, Integer> {

    @Query("select i from IDE i where i.IDEName = ?1")
    Optional<IDE> getByName(String name);
}
