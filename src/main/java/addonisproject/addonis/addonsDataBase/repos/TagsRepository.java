package addonisproject.addonis.addonsDataBase.repos;

import addonisproject.addonis.addonsDataBase.models.addons.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * The TagsRepository is used for managing entries to the tags table in our database. In addition
 * to the predefined by JPA methods, it provides functionality for getting tag by name and checking if the
 * tag name already exists
 */
@Repository
public interface TagsRepository extends JpaRepository<Tag, Integer> {

    @Query("select t from Tag t where t.name = ?1")
    Optional<Tag> getByName(String name);

    @Query("select case when (count(t) > 0) then true else false end from Tag t where t.name = :name")
    Boolean checkTagNameExists(String name);
}
