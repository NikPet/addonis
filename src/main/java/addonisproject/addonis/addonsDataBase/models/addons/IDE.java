package addonisproject.addonis.addonsDataBase.models.addons;

import javax.persistence.*;

@Entity
@Table(name = "ides")
public class IDE {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "IDE_name")
    private String IDEName;

    @Column(name = "picture")
    private String picture;

    public IDE() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIDEName() {
        return IDEName;
    }

    public void setIDEName(String IDEName) {
        this.IDEName = IDEName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
