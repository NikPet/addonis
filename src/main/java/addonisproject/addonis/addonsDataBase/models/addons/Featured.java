package addonisproject.addonis.addonsDataBase.models.addons;

import javax.persistence.*;

@Entity
@Table(name = "featured")
public class Featured {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "addon_id")
    private Addon addon;

    public Featured() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Addon getAddon() {
        return addon;
    }

    public void setAddon(Addon addon) {
        this.addon = addon;
    }
}
