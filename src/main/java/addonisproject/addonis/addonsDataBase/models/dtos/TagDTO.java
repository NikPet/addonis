package addonisproject.addonis.addonsDataBase.models.dtos;

import javax.validation.constraints.NotBlank;

/**
 * The TagDTO class is used when users add tags to an addon.
 * It holds the String value of the tag
 */
public class TagDTO {

    @NotBlank
    private String name;

    public TagDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
