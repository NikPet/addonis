package addonisproject.addonis.addonsDataBase.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static addonisproject.addonis.utils.Constants.PASSWORD_LENGTH_ERROR_MESSAGE;
import static addonisproject.addonis.utils.Constants.USERNAME_LENGTH_ERROR_MESSAGE;

/**
 * The UserDTO is used for transferring data when users register new accounts.
 */
public class UserDTO {

    @NotBlank
    @Size(min = 6, max = 20, message = USERNAME_LENGTH_ERROR_MESSAGE)
    private String username;

    @NotBlank
    @Size(min = 8, message = PASSWORD_LENGTH_ERROR_MESSAGE)
    private String password;

    private String repeatPassword;

    @NotBlank
    private String email;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;


    public UserDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
