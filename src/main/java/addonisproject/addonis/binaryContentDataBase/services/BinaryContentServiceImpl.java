package addonisproject.addonis.binaryContentDataBase.services;

import addonisproject.addonis.binaryContentDataBase.models.BinaryContent;
import addonisproject.addonis.binaryContentDataBase.repos.BinaryContentRepository;
import addonisproject.addonis.binaryContentDataBase.services.contracts.BinaryContentService;
import addonisproject.addonis.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

/**
 * Implementation of BinaryContentService interface, responsible for saving the uploaded binary content in
 * the database once the addon is created
 */
@Service
public class BinaryContentServiceImpl implements BinaryContentService {

    private BinaryContentRepository binaryContentRepository;

    @Autowired
    public BinaryContentServiceImpl(BinaryContentRepository binaryContentRepository) {
        this.binaryContentRepository = binaryContentRepository;
    }

    @Override
    public BinaryContent getById(int id) {
        return binaryContentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException
                        (String.format(Constants.ENTITY_NOT_FOUND_ERROR_MESSAGE, Constants.BINARY_TYPE)));
    }

    @Override
    public void createBinaryContent(BinaryContent binaryContent) {
        binaryContentRepository.saveAndFlush(binaryContent);
    }
}
