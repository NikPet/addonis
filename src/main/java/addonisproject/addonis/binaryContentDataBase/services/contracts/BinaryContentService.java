package addonisproject.addonis.binaryContentDataBase.services.contracts;

import addonisproject.addonis.binaryContentDataBase.models.BinaryContent;

public interface BinaryContentService {

    BinaryContent getById(int id);

    void createBinaryContent(BinaryContent binaryContent);
}
