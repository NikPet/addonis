package addonisproject.addonis.binaryContentDataBase.repos;

import addonisproject.addonis.binaryContentDataBase.models.BinaryContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * BinaryContentRepository is used to manage entries to the binary_contents table.
 * It provides the predefined by JPA methods.
 */
@Repository
public interface BinaryContentRepository extends JpaRepository<BinaryContent, Integer>{
}
