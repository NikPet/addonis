package addonisproject.addonis.exceptions;

/**
 * Custom exception
 */
public class SizeExceededException extends RuntimeException{

    public SizeExceededException(String message) {
        super(message);
    }

    public SizeExceededException(int limit,String type,String exceededLimitList) {
        super(String.format("The limit of %s %s in %s reached", limit, type,exceededLimitList));
    }
}
