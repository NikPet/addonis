package addonisproject.addonis.exceptions;

public class FileProblemException extends RuntimeException {
    public FileProblemException(String message){
        super(message);
    }
}
