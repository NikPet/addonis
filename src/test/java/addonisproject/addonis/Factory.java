package addonisproject.addonis;

import addonisproject.addonis.addonsDataBase.models.addons.*;
import addonisproject.addonis.addonsDataBase.models.dtos.AddonCardDTO;
import addonisproject.addonis.addonsDataBase.models.dtos.TagDTO;

import java.util.*;

public class Factory {


    public static Addon createApprovedAddon() {
        Addon addon = new Addon();
        addon.setId(1);
        addon.setName("Test Addon");
        addon.setDescription("test");
        addon.setCreator(createUser());
        Set<Tag> tags = new HashSet<>();
        addon.setTags(tags);
        addon.setNumberDownloads(1);
        addon.setUploadDate(new Date());
        addon.setBinaryContent(1);
        addon.setOriginLink("https://github.com/nikipet/testrepo");
        addon.setStatus(createApprovedStatus());
        addon.setIde(createIde());
        return addon;
    }
    public static Addon createApprovedAddon(int id, String name) {
        Addon addon = new Addon();
        addon.setName(name);
        addon.setId(id);
        addon.setCreator(createUser());
        addon.setOriginLink("https://github.com/nikipet/testrepo");
        addon.setIde(createIde());
        addon.setStatus(createApprovedStatus());
        return addon;
    }

    public static Addon createApprovedAddon(int id, String name, IDE ide) {
        Addon addon = new Addon();
        addon.setName(name);
        addon.setId(id);
        addon.setCreator(createUser());
        addon.setOriginLink("https://github.com/nikipet/testrepo");
        addon.setIde(ide);
        addon.setStatus(createApprovedStatus());
        return addon;
    }


    public static Addon createPendingAddon() {
        Addon addon = new Addon();
        addon.setId(2);
        addon.setName("Test Pending Addon");
        addon.setDescription("test");
        addon.setCreator(createUser());
        Set<Tag> tags = new HashSet<>();
        addon.setTags(tags);
        addon.setNumberDownloads(1);
        addon.setUploadDate(new Date());
        addon.setBinaryContent(2);
        addon.setOriginLink("https://github.com/nikipet/testrepo");
        addon.setStatus(createPendingStatus());
        addon.setIde(createIde());
        return addon;
    }

    public static IDE createIde() {
        IDE ide = new IDE();
        ide.setId(1);
        ide.setIDEName("NetBeans");
        ide.setPicture("netbeans.png");
        return ide;
    }
    public static IDE createIde(String name) {
        IDE ide = new IDE();
        ide.setId(1);
        ide.setIDEName(name);
        return ide;
    }

    public static Status createPendingStatus() {
        Status status = new Status();
        status.setId(1);
        status.setStatus("pending");
        return status;
    }

    public static Status createApprovedStatus() {
        Status status = new Status();
        status.setId(2);
        status.setStatus("approved");
        return status;
    }

    public static Status createDeletedStatus() {
        Status status = new Status();
        status.setId(3);
        status.setStatus("deleted");
        return status;
    }

    public static UserDetails createUser() {
        UserDetails user = new UserDetails();
        user.setId(1);
        user.setUsername("TestUserName");
        user.setLastName("TestLastName");
        user.setFirstName("TestFirstName");
        user.setEmail("TestEmail@test.com");
        user.setEnabled(true);
        return user;
    }
    public static UserDetails createUser(String userName) {
        UserDetails user = new UserDetails();
        user.setId(1);
        user.setUsername(userName);
        user.setLastName("TestLastName");
        user.setFirstName("TestFirstName");
        user.setEmail("TestEmail@test.com");
        user.setEnabled(true);
        return user;
    }
    public static UserDetails createUser(String userName,String Email) {
        UserDetails user = new UserDetails();
        user.setId(1);
        user.setUsername(userName);
        user.setLastName("TestLastName");
        user.setFirstName("TestFirstName");
        user.setEmail(Email);
        user.setEnabled(true);
        return user;
    }

    public static Tag createTag() {
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("testTag");
        return tag;
    }

    public static TagDTO createTagDto(){
        TagDTO tagDto = new TagDTO();
        tagDto.setName("testTag");
        return tagDto;
    }

    public static Rating createRating(){
        Rating rating = new Rating();
        rating.setId(1);
        rating.setAddon(createApprovedAddon());
        rating.setUserDetails(createUser());
        rating.setRating(3);
        return rating;
    }

    public static List<Addon> getTenFeaturedAddons(){
        Addon addon1 = Factory.createApprovedAddon(1, "dTest1", createIde("IDE1"));
        Addon addon2 = Factory.createApprovedAddon(2, "cTest2", createIde("IDE2"));
        Addon addon3 = Factory.createApprovedAddon(3, "aTest3", createIde("IDE1"));
        Addon addon4 = Factory.createApprovedAddon(4, "bTest4", createIde("IDE3"));
        Addon addon5 = Factory.createApprovedAddon(5, "fTest1", createIde("IDE1"));
        Addon addon6 = Factory.createApprovedAddon(6, "eTest2", createIde("IDE2"));
        Addon addon7 = Factory.createApprovedAddon(7, "nTest3", createIde("IDE1"));
        Addon addon8 = Factory.createApprovedAddon(8, "mTest4", createIde("IDE3"));
        Addon addon9 = Factory.createApprovedAddon(9, "pTest1", createIde("IDE1"));
        Addon addon10 = Factory.createApprovedAddon(10, "qTest2", createIde("IDE2"));
        return Arrays.asList(addon1, addon2,addon3,addon4,addon5,addon6,addon7,addon8,addon9,addon10);
    }


    public static List<Addon> getFilterableApprovedAddons() {
        Addon addon1 = Factory.createApprovedAddon(1, "dTest1", createIde("IDE1"));
        Addon addon2 = Factory.createApprovedAddon(2, "cTest2", createIde("IDE2"));
        Addon addon3 = Factory.createApprovedAddon(3, "aTest3", createIde("IDE1"));
        Addon addon4 = Factory.createApprovedAddon(4, "bTest4", createIde("IDE3"));
        addon1.setNumberDownloads(4);
        addon2.setNumberDownloads(1);
        addon3.setNumberDownloads(3);
        addon4.setNumberDownloads(2);
        List<Addon> addonList = new ArrayList<>();
        addonList.add(addon1);
        addonList.add(addon2);
        addonList.add(addon3);
        addonList.add(addon4);
        return addonList;
    }

    public static List<Addon> getSortedByNameAddons() {
        Addon addon1 = Factory.createApprovedAddon(1, "dTest1", createIde("IDE1"));
        Addon addon2 = Factory.createApprovedAddon(2, "cTest2", createIde("IDE2"));
        Addon addon3 = Factory.createApprovedAddon(3, "aTest3", createIde("IDE1"));
        Addon addon4 = Factory.createApprovedAddon(4, "bTest4", createIde("IDE3"));
        List<Addon> addonList = new ArrayList<>();
        addonList.add(addon3);
        addonList.add(addon4);
        addonList.add(addon2);
        addonList.add(addon1);
        return addonList;
    }

    public static List<Addon> getSortedByDownloads() {
        Addon addon1 = Factory.createApprovedAddon(1, "dTest1", createIde("IDE1"));
        Addon addon2 = Factory.createApprovedAddon(2, "cTest2", createIde("IDE2"));
        Addon addon3 = Factory.createApprovedAddon(3, "aTest3", createIde("IDE1"));
        Addon addon4 = Factory.createApprovedAddon(4, "bTest4", createIde("IDE3"));
        addon1.setNumberDownloads(4);
        addon2.setNumberDownloads(1);
        addon3.setNumberDownloads(3);
        addon4.setNumberDownloads(2);
        List<Addon> addonList = new ArrayList<>();
        addonList.add(addon2);
        addonList.add(addon4);
        addonList.add(addon3);
        addonList.add(addon1);
        return addonList;
    }


    public static List<Addon> getPopularAddons() {
        Addon addon1 = Factory.createApprovedAddon(1, "dTest1", createIde("IDE1"));
        Addon addon2 = Factory.createApprovedAddon(2, "cTest2", createIde("IDE2"));
        Addon addon3 = Factory.createApprovedAddon(3, "aTest3", createIde("IDE1"));
        Addon addon4 = Factory.createApprovedAddon(4, "bTest4", createIde("IDE3"));
        Addon addon5 = Factory.createApprovedAddon(5, "bTest5", createIde("IDE2"));
        Addon addon6 = Factory.createApprovedAddon(6, "fTest6", createIde("IDE2"));
        Addon addon7 = Factory.createApprovedAddon(7, "fTest7", createIde("IDE7"));

        addon1.setNumberDownloads(4);
        addon2.setNumberDownloads(1);
        addon3.setNumberDownloads(3);
        addon4.setNumberDownloads(2);
        addon5.setNumberDownloads(6);
        addon6.setNumberDownloads(16);
        addon7.setNumberDownloads(9);

        List<Addon> addonList = new ArrayList<>();
        addonList.add(addon2);
        addonList.add(addon4);
        addonList.add(addon3);
        addonList.add(addon1);
        addonList.add(addon5);
        addonList.add(addon6);
        addonList.add(addon7);
        return addonList;
    }

    public static List<Addon> getAllAddons() {
        Addon addon1 = Factory.createApprovedAddon();
        Addon addon2 = Factory.createApprovedAddon();
        Addon addon3 = Factory.createPendingAddon();
        Addon addon4 = Factory.createApprovedAddon();
        Addon addon5 = Factory.createPendingAddon();
        List<Addon> addonList = new ArrayList<>();
        addonList.add(addon1);
        addonList.add(addon2);
        addonList.add(addon3);
        addonList.add(addon4);
        addonList.add(addon5);
        return addonList;
    }

    public static List<AddonCardDTO> getFiveAddonCards()
    {
        AddonCardDTO addonCardDTO1=new AddonCardDTO();
        AddonCardDTO addonCardDTO2=new AddonCardDTO();
        AddonCardDTO addonCardDTO3=new AddonCardDTO();
        AddonCardDTO addonCardDTO4=new AddonCardDTO();
        AddonCardDTO addonCardDTO5=new AddonCardDTO();

        List<AddonCardDTO> addonCardDTOList=new ArrayList<>();
        addonCardDTOList.add(addonCardDTO1);
        addonCardDTOList.add(addonCardDTO2);
        addonCardDTOList.add(addonCardDTO3);
        addonCardDTOList.add(addonCardDTO4);
        addonCardDTOList.add(addonCardDTO5);
        return addonCardDTOList;
    }
}
