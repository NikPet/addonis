package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.addons.IDE;
import addonisproject.addonis.addonsDataBase.repos.IDERepository;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static addonisproject.addonis.Factory.createIde;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class IDEServiceImplTests {

    @Mock
    IDERepository ideRepository;

    @InjectMocks
    IDEServiceImpl ideService;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void getAllIdesShould_CallRepository(){
        //      Arrange, Act
        ideService.getAll();
        //        Assert
        Mockito.verify(ideRepository,Mockito.times(1))
                .findAll();
    }

    @Test
    public void getIdeByNameShould_ReturnIde_WhenIdeExists(){
        //    Arrange
        IDE expected = createIde();
        Mockito.when(ideRepository.getByName(anyString()))
                .thenReturn(Optional.of(expected));
        //        Act
        IDE returned = ideService.getIdeByName(anyString());
        //        Assert
        Assert.assertSame(expected,returned);
    }

    @Test
    public void getIdeByNameShould_CallRepository(){
        //         Arrange
        IDE ide = createIde();
        Mockito.when(ideRepository.getByName(anyString()))
                .thenReturn(Optional.of(ide));
        //         Act
        ideService.getIdeByName(anyString());
        //        Assert
        Mockito.verify(ideRepository,Mockito.times(1))
                .getByName(anyString());
    }

    @Test
    public void getIdeByNameShould_Throw_WhenIdeDoesNotExists(){
        //    Arrange, Act , Assert
        exception.expect(EntityNotFoundException.class);
        ideService.getIdeByName(anyString());
    }
}
