package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.addons.Addon;
import addonisproject.addonis.addonsDataBase.repos.AddonsRepository;
import addonisproject.addonis.exceptions.ApiConnectionException;
import addonisproject.addonis.addonsDataBase.models.Commit;

import static addonisproject.addonis.Factory.createApprovedAddon;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
public class APIServicesImplTests {

    @Mock
    AddonsRepository addonsRepository;

    @InjectMocks
    APIServiceImpl mockService;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void getLastCommitShould_ReturnDate_WhenInputIsCorrect() throws ParseException {
        //Arrange
        Addon addon = createApprovedAddon();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date expectedDate = format.parse("2020-04-09T13:36:13Z");
        Commit expectedCommit = new Commit(expectedDate, "created Test file");
        //Act
        Commit lastCommit = mockService.getLastCommit(addon);
        //Assert
        assertThat(lastCommit)
                .usingRecursiveComparison()
                .isEqualTo(expectedCommit);
    }

    @Test
        public void getCountIssuesShould_ReturnCorrectIssuesCount() {
            //Arrange
            Addon addon = createApprovedAddon();
            //Act
            int result = mockService.getCountIssues(addon);
            //Assert
            Assert.assertEquals(0, result);
        }

        @Test
        public void getCountPullsShould_ReturnCorrectPullsCount() {
            //Arrange
            Addon addon = createApprovedAddon();
            //Act
            int result = mockService.getCountIssues(addon);
            //Assert
            Assert.assertEquals(0, result);
    }

    @Test
    public void getReadMeShould_ThrownException_WhenConnectionFailed() {
        //Arrange
        Addon addon = createApprovedAddon();
        addon.setOriginLink(addon.getOriginLink()+"salt");
        //Act,Assert
         exception.expect(ApiConnectionException.class);
         mockService.getReadMe(addon);
    }


}
