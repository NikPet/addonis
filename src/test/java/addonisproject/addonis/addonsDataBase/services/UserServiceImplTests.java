package addonisproject.addonis.addonsDataBase.services;

import addonisproject.addonis.addonsDataBase.models.addons.UserDetails;
import addonisproject.addonis.addonsDataBase.repos.UserDetailsRepository;
import addonisproject.addonis.exceptions.UserRegistrationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static addonisproject.addonis.Factory.createUser;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UserDetailsRepository userDetailsRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    UserDetailsManager userDetailsManager;

    @InjectMocks
    UsersServiceImpl mockService;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void getAllUsers_ShouldCallRepository() {
        //Arrange
        List<UserDetails> resultList = new ArrayList<>();
        UserDetails fakeUser = createUser();
        resultList.add(fakeUser);
        Mockito.when(userDetailsRepository.findAll()).thenReturn((resultList));
        //Act
        mockService.getAllUsers();
        //Assert
        verify(userDetailsRepository,
                Mockito.times(1)).findAll();
    }

    @Test
    public void CreateUser_ShouldCallRepository_WhenInputIsCorrect() {
        //Arrange
        UserDetails newUser = createUser("Todor", "Test2@abv.bg");
        Mockito.when(passwordEncoder.encode(anyString())).thenReturn("Done");
        //Act
        mockService.createUser(newUser, "Password", "Password");
        //Assert
        verify(userDetailsRepository,
                Mockito.times(1)).save(newUser);
    }

    @Test
    public void createUserShould_ThrowUserRegistrationException_WhenUserNameIsTaken() {
        //Arrange
        List<UserDetails> registeredUsersList = new ArrayList<>();
        UserDetails registeredUser = createUser("Ivan");
        registeredUsersList.add(registeredUser);
        UserDetails newUser = createUser("Ivan");
        Mockito.when(userDetailsRepository.findAll()).thenReturn(registeredUsersList);
        //Act,Assert
        exception.expect(UserRegistrationException.class);
        mockService.createUser(newUser, "Password", "Password");

    }

    @Test
    public void createUserShould_ThrowUserRegistrationException_WhenEmailIsTaken() {
        //Arrange
        List<UserDetails> registeredUsersList = new ArrayList<>();
        UserDetails registeredUser = createUser("Ivan", "Test@abv.bg");
        registeredUsersList.add(registeredUser);
        UserDetails newUser = createUser("Todor", "Test@abv.bg");
        Mockito.when(userDetailsRepository.findAll()).thenReturn(registeredUsersList);
        //Act,Assert
        exception.expect(UserRegistrationException.class);
        mockService.createUser(newUser, "Password", "Password");
    }

    @Test
    public void createUserShould_ThrowUserRegistrationException_WhenPasswordsDoNotMatch() {
        //Arrange
        List<UserDetails> registeredUsersList = new ArrayList<>();
        UserDetails registeredUser = createUser("Ivan", "Test@abv.bg");
        registeredUsersList.add(registeredUser);
        UserDetails newUser = createUser("Todor", "Test2@abv.bg");
        Mockito.when(userDetailsRepository.findAll()).thenReturn(registeredUsersList);
        //Act,Assert
        exception.expect(UserRegistrationException.class);
        mockService.createUser(newUser, "Password1", "Password");
    }

    @Test
    public void getUserByNameShould_CallRepository() {
        //Arrange
        UserDetails registeredUser = createUser();
        Mockito.when(userDetailsRepository.getByName(anyString())).thenReturn(java.util.Optional.of(registeredUser));
        //Act
        mockService.getUserByName("TestUserName");
        //Assert
        verify(userDetailsRepository,
                Mockito.times(1)).getByName("TestUserName");
    }

    @Test
    public void getUserByNameShould_ThrowEntityNotFoundException_WhenNoUserIsFound() {
        //Arrange
        Mockito.when(userDetailsRepository.getByName(anyString())).thenReturn(java.util.Optional.empty());
        //Act,Assert
        exception.expect(EntityNotFoundException.class);
        mockService.getUserByName("TestUserName");
    }

    @Test
    public void getUserByIDShould_CallRepository() {
        //Arrange
        UserDetails registeredUser = createUser();
        Mockito.when(userDetailsRepository.findById(anyInt())).
                thenReturn(java.util.Optional.of(registeredUser));
        //Act
        mockService.getUserById(1);
        //Assert
        verify(userDetailsRepository,
                Mockito.times(1)).findById(1);
    }

    @Test
    public void getUserByIDShould_ThrowEntityNotFoundException_WhenNoUserIsFound() {
        //Arrange
        Mockito.when(userDetailsRepository.findById(anyInt())).
                thenReturn(java.util.Optional.empty());
        //Act,Assert
        exception.expect(EntityNotFoundException.class);
        mockService.getUserById(1);
    }

    @Test
    public void DeleteUserShould_CallRepository() {
        //Arrange
        UserDetails userDetails=createUser();
        //Act
        mockService.deleteUser(userDetails);
        //Assert
        verify(userDetailsRepository,
                Mockito.times(1)).save(userDetails);
    }

}




